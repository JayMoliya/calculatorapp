package com.example.calculatorapp

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    var operator = "+"
    var oldNumber = "0"
    var isNewOperator = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    fun numberEvent(view: View) {
        if (isNewOperator)
            textInput.setText("")
        isNewOperator = false
        var input = textInput.text.toString()
        val btnSelect: Button = view as Button

        when (btnSelect.id) {
            btnZero.id -> input+= "0"
            btnOne.id -> {
                input += "1"
            }
            btnTwo.id -> {
                input += "2"
            }
            btnThree.id -> {
                input += "3"
            }
            btnFour.id -> {
                input += "4"
            }
            btnFive.id -> {
                input += "5"
            }
            btnSix.id -> {
                input += "6"
            }
            btnSeven.id -> {
                input += "7"
            }
            btnEight.id -> {
                input += "8"
            }
            btnNine.id -> {
                input += "9"
            }
            btnDot.id -> {
                input += "."
            }
            btnBack.id -> {
                if (input.isNotEmpty())
                    input = input.substring(0, input.length - 1)
                else input += "0"
            }
        }
        textInput.setText(input)
        textInput.setSelection(textInput.text.length)
    }

    fun operatorEvent(view: View) {
        isNewOperator = true
        oldNumber = textInput.text.toString()
        val btnSelect = view as Button

        when (btnSelect.id) {
            btnAdd.id -> operator = "+"
            btnSubtract.id -> operator = "-"
            btnDivide.id -> operator = "/"
            btnMultiply.id -> operator = "*"
        }

    }

    fun actionEvent(view: View) {
        val input = textInput.text.toString()
        var result = 0.0

        when (operator) {
            "+" -> result = oldNumber.toDouble() + input.toDouble()
            "-" -> result = oldNumber.toDouble() - input.toDouble()
            "/" -> result = oldNumber.toDouble() / input.toDouble()
            "*" -> result = oldNumber.toDouble() * input.toDouble()
        }

        textInput.setText(result.toString())
        textInput.setSelection(textInput.text.length)

    }

    fun clearEvent(view: View) {
        textInput.setText("0")
        isNewOperator = true
        }

    fun percentEvent(view: View) {
        val number = textInput.text.toString().toDouble()/100
        textInput.setText(number.toString())
        textInput.setSelection(textInput.text.length)
        isNewOperator = true
    }
}
